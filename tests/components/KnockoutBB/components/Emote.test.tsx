import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { customRender, screen } from '../../../custom_renderer';
import Parser from '../../../../src/components/KnockoutBB/Parser';

/*
asdf :unknown-emote: asdf
plaintext :angry: ppp
[b]inside bold tag :buddy: bbb[/b]
[i]inside italic :cat: iii[/i]
[u] inside underline :cool: uuu[/u]
[s]inside strikethrough :cry: sss[/s]
[url href="https://knockout.chat"]Doesn't replace url text :dead: url[/url]
[code inline]doesn't replace in inline code :weeb: code[/code]
[code]Doesn't replace in block code :vomit: code[/code]
[h1]Works in headers :disgust: h1[/h1]
[h2]Works in headers :evil: h2[/h2]
[blockquote]Works in blockquotes :frown: blockquote[/blockquote]
[quote username=":v: doesn't replace names"]Works in normal quotes :glare: quote [/quote]
[collapse title="works in titles of collapse :hypeisreal:"]And inside :nsfw:
[collapse title=""]And nested :hypeisnotreal:[/collapse][/collapse]
[ul][li]Unordered list :pwn: li[/li][/ul]
[ol][li]Ordered list :scream: li[/li][/ol]
[noparse]doesn't work in noparse :ohno: noparse[/noparse]
*/

describe('Emote component', () => {
  it("doesn't convert unknown emotes", () => {
    customRender(<Parser content="#marker :unknownemote: asdf" />);
    expect(screen.findByText(':unknownemote:')).not.toBeNull();
  });

  it('convert emotes in bold tags', () => {
    customRender(<Parser content="[b]:buddy:[/b]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in italic tags', () => {
    customRender(<Parser content="[i]:buddy:[/i]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in underline tags', () => {
    customRender(<Parser content="[u]:buddy:[/u]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in strikethrough tags', () => {
    customRender(<Parser content="[s]:buddy:[/s]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it("doesn't convert emotes in url tags", () => {
    customRender(
      <Parser
        content={`[url href="https://knockout.chat"]Doesn't replace url text :dead: url[/url]`}
      />
    );
    expect(screen.findByText(':dead:')).not.toBeNull();
  });

  it("doesn't convert emotes in inline code tags", () => {
    customRender(
      <Parser content={`[code inline]doesn't replace in inline code :weeb: code[/code]`} />
    );
    expect(screen.findByText(':weeb:')).not.toBeNull();
  });

  it("doesn't convert emotes in code tags", () => {
    customRender(<Parser content={`[code]doesn't replace in inline code :weeb: code[/code]`} />);
    expect(screen.findByText(':weeb:')).not.toBeNull();
  });

  it('convert emotes in h1 tags', () => {
    customRender(<Parser content="[h1]:buddy:[/h1]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in h2 tags', () => {
    customRender(<Parser content="[h2]:buddy:[/h2]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in blockquote tags', () => {
    customRender(<Parser content="[blockquote]:buddy:[/blockquote]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in quote tags', () => {
    customRender(<Parser content="[quote]:buddy:[/quote]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in collapse tags', () => {
    customRender(<Parser content="[collapse]:buddy:[/collapse]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in collapse tags in title', () => {
    customRender(<Parser content='[collapse title=":buddy:"]text[/collapse]' />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it('convert emotes in list tags', () => {
    customRender(<Parser content="[ul][li]Unordered list :buddy: li[/li][/ul]" />);
    expect(screen.getByAltText(':buddy:')).not.toBeNull();
  });

  it("doesn't convert emotes in noparse tags", () => {
    customRender(
      <Parser content={`[noparse]doesn't replace in inline code :buddy: code[/noparse]`} />
    );
    expect(screen.findByText(':buddy:')).not.toBeNull();
  });

  it("doesn't convert emotes in unknown tags", () => {
    customRender(
      <Parser content={`[asdfasdf]doesn't replace in inline code :buddy: code[/asdfasdf]`} />
    );
    expect(screen.findByText(':buddy:')).not.toBeNull();
  });
});
