import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import CollapseBB from '../../../../src/components/KnockoutBB/components/CollapseBB';
import { customRender, screen, fireEvent } from '../../../custom_renderer';

describe('Collapsible component', () => {
  it('displays the title', () => {
    customRender(
      <CollapseBB title="Custom title">
        <p>Collapsed content</p>
      </CollapseBB>
    );
    expect(screen.queryByText('Custom title')).toBeInTheDocument();
  });

  it('hides content when collapsed', () => {
    customRender(
      <CollapseBB title="Custom title">
        <p>Collapsed content</p>
      </CollapseBB>
    );
    expect(screen.queryByText('Collapsed content')).not.toBeVisible();
  });

  it('displays content when clicked', () => {
    const collapse = customRender(
      <CollapseBB title="Custom title">
        <p>Collapsed content</p>
      </CollapseBB>
    );
    fireEvent.click(collapse.getByRole('button'));
    expect(screen.queryByText('Collapsed content')).toBeVisible();
  });
});
