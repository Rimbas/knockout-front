import axios from 'axios';
import React from 'react';
import userEvent from '@testing-library/user-event';
import UserSearchModal from '../../../../src/views/MessagesPage/components/UserSearchModal';
import { customRender } from '../../../custom_renderer';
import { BASIC_USER, GOLD_USER } from '../../../../src/utils/roleCodes';

jest.mock('axios');
describe('UserSearchModal component', () => {
  const setModalOpen = jest.fn();
  const selectUser = jest.fn();

  const users = [
    { id: 2, username: 'Rick', avatarUrl: '', role: { code: BASIC_USER } },
    { id: 3, username: 'Laura', avatarUrl: '', role: { code: GOLD_USER } },
  ];

  it('displays the modal', async () => {
    const { queryByText } = customRender(
      <UserSearchModal modalOpen setModalOpen={setModalOpen} selectUser={selectUser} />
    );
    expect(queryByText('New message')).not.toBeNull();
  });

  it('displays search results', async () => {
    axios.get.mockResolvedValue({ data: { users } });
    const { findByText, queryByPlaceholderText } = customRender(
      <UserSearchModal modalOpen setModalOpen={setModalOpen} selectUser={selectUser} />
    );
    const input = queryByPlaceholderText('Search for users');
    expect(input).not.toBeNull();

    userEvent.type(input, 'stuff');
    await findByText('Rick');
    const user = await findByText('Laura');

    userEvent.click(user);
    expect(selectUser).toBeCalled();
    expect(setModalOpen).toBeCalled();
  });
});
