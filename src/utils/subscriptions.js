import { setSubscriptions } from '../state/subscriptions';
import { unreadPostPage } from './postsPerPage';

const updateSubscriptions = (dispatch, subscriptions) => {
  const subscriptionState = { threads: {}, count: 0 };
  subscriptions.forEach((subscription) => {
    if (subscription.unreadPosts) {
      let subscriptionThread = subscription.thread;
      // handle short response format, where the thread object is spread as shallow attributes
      if (!subscriptionThread) {
        subscriptionThread = {
          title: subscription.threadTitle,
          postCount: subscription.threadPostCount,
          iconId: subscription.threadIcon,
          locked: subscription.threadLocked,
        };
      }
      subscriptionState.threads[subscription.id] = {
        title: subscriptionThread.title,
        page: unreadPostPage(subscription.unreadPosts, subscriptionThread.postCount),
        count: subscription.unreadPosts,
        postId: subscription.firstUnreadId,
        postNum: subscriptionThread.postCount - subscription.unreadPosts,
        iconId: subscriptionThread.iconId,
        locked: subscriptionThread.locked,
      };
      subscriptionState.count += subscription.unreadPosts;
    }
  });
  dispatch(setSubscriptions(subscriptionState));
};

export default updateSubscriptions;
