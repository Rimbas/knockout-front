import { useEffect } from 'react';

export default (src, id = '', waitForValue = false, value: string | object[] = '') => {
  useEffect(() => {
    let script;
    if ((!waitForValue || value.length) && (!id || !document.getElementById(id))) {
      const first = document.getElementsByTagName('script')[0];
      script = document.createElement('script');
      script.id = id;
      script.src = src;
      script.async = true;
      first.parentNode?.insertBefore(script, first);
    }
    return () => {
      if (script) script.remove();
    };
  }, [src, value]);
};
