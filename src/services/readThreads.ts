import { AlertsResponse, CreateAlertRequest } from 'knockout-schema';
import { authPost, authDelete, authGet } from './common';

export const createReadThreadRequest = async (
  threadId: number,
  lastPostNumber: number
): Promise<void> => {
  const requestBody: CreateAlertRequest = {
    lastPostNumber,
    threadId,
  };

  await authPost({ url: '/v2/read-threads', data: requestBody });
};

export const deleteReadThread = async (threadId: number) => {
  return authDelete({ url: `/v2/read-threads/${threadId}` });
};

export const getReadThreads = async (): Promise<AlertsResponse> => {
  try {
    return (await authGet({ url: '/v2/read-threads' })).data;
  } catch (err) {
    return { alerts: [], totalAlerts: 0, ids: [] };
  }
};
