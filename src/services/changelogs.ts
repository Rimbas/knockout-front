/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import { KnockoutCommit } from 'knockout-schema';
import config from '../../config';

export const getChangelog = async (page: number = 1): Promise<Array<KnockoutCommit>> => {
  try {
    const results = await axios.get(`${config.apiHost}/v2/changelog/${page}`);
    return results.data;
  } catch (err) {
    return [];
  }
};
