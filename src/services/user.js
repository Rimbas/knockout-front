/* eslint-disable no-console */
/* eslint-disable camelcase */
import axios from 'axios';

import config from '../../config';

import { authPut, authGet, authDelete, authPost } from './common';
import { pushSmartNotification } from '../utils/notification';
import { saveBannedMessageToStorage, clearBannedMessageFromStorage } from '../utils/bannedStorage';
import { MODERATOR_ROLES } from '../utils/roleCodes';

export const updateUser = (data, id) => authPut({ url: `/v2/users/${id}`, data });

export const checkUsername = (username, id) =>
  authPut({ url: `/v2/users/${id}/check-username`, data: { username } });

export const loadUserFromStorage = () => {
  const user = JSON.parse(localStorage.getItem('currentUser'));

  if (user) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { username, avatar_url, avatarUrl, backgroundUrl, id, createdAt, role, title } = user;

    return {
      username,
      avatarUrl: avatarUrl || avatar_url,
      backgroundUrl,
      id,
      createdAt,
      role,
      title,
    };
  }
  return null;
};

export const saveUserToStorage = (data) => {
  const { username, avatarUrl, backgroundUrl, id, createdAt, role, title } = data.user;
  const user = JSON.stringify({
    username,
    avatarUrl,
    backgroundUrl,
    id,
    createdAt,
    role,
    title,
  });

  localStorage.setItem('currentUser', user);
};

export const googleLogin = async (googleResponse) => {
  const res = await axios.get(`${config.apiHost}/auth/google`, {
    headers: {
      gtoken: googleResponse.tokenId,
    },
  });

  const { data } = res;

  return data;
};

export const removeUserFromStorage = () => {
  localStorage.removeItem('currentUser');
};

export const searchUsers = async (query, modTable = false) => {
  // if logged in, get the user with auth
  const user = localStorage.getItem('currentUser');
  let res;
  let endpoint = `/users/?filter=${query}`;
  if (modTable) endpoint += '&modTable=true';
  if (user) {
    res = await authGet({ url: endpoint });
  } else {
    res = await axios.get(`${config.apiHost}${endpoint}`);
  }

  const { data } = res;
  return data.users;
};

export const getUser = async (userId) => {
  // if logged in, get the user with auth
  const user = localStorage.getItem('currentUser');
  if (user) {
    const res = await authGet({ url: `/user/${userId}` });
    const { data } = res;
    return data;
  }

  const res = await axios.get(`${config.apiHost}/user/${userId}`);
  const { data } = res;
  return data;
};

export const getUserBans = async (userId) => {
  const res = await axios.get(`${config.apiHost}/user/${userId}/bans`);
  const { data } = res;
  return data;
};

export const getUserTopRatings = async (userId) => {
  try {
    const res = await axios.get(`${config.apiHost}/user/${userId}/topRatings`);
    const { data } = res;
    return data;
  } catch (error) {
    return [];
  }
};

export const getPreviousUsernames = async (userId) => {
  const res = await axios.get(`${config.apiHost}/v2/users/${userId}/previous-usernames`);
  const { data } = res;
  return data;
};

export const getUserProfile = async (userId, silent = false) => {
  try {
    const endpoint = `${config.apiHost}/v2/users/${userId}/profile`;
    let res;
    if (silent) {
      res = { data: await (await fetch(endpoint)).json() };
    } else {
      res = await axios.get(endpoint);
    }
    const { data } = res;
    return data;
  } catch (error) {
    pushSmartNotification(error);
    return [];
  }
};

export const updateUserProfile = async (userId, data) => {
  const res = await authPut({
    url: `/v2/users/${userId}/profile`,
    data,
  });

  return pushSmartNotification(res.data);
};

export const getUserProfileComments = async (userId, page = 1) => {
  const res = await axios.get(`${config.apiHost}/v2/users/${userId}/profile/comments?page=${page}`);
  const { data } = res;
  return data;
};

export const createUserProfileComment = async (userId, content) => {
  const res = await authPost({
    url: `/v2/users/${userId}/profile/comments`,
    data: { content },
  });
  return res.data;
};

export const deleteUserProfileComment = async (userId, commentId) => {
  try {
    const res = await authDelete({
      url: `/v2/users/${userId}/profile/comments/${commentId}`,
    });
    return pushSmartNotification(res.data);
  } catch (error) {
    return pushSmartNotification({ error: 'Could not delete profile comment.' });
  }
};

export const updateUserBackground = async (userId, data) => {
  try {
    const formData = new FormData();
    formData.append('image', data.image);
    formData.append('type', data.type);

    const res = await authPut({
      url: `/v2/users/${userId}/profile/background`,
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    return pushSmartNotification(res.data);
  } catch (error) {
    pushSmartNotification({ error: 'Could not update profile. Is your image under 2MB?' });
    return {};
  }
};

export const updateUserHeader = async (userId, image) => {
  try {
    const formData = new FormData();
    formData.append('image', image);

    const res = await authPut({
      url: `/v2/users/${userId}/profile/header`,
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    return res.data;
  } catch (error) {
    pushSmartNotification({ error: 'Could not update profile header. Is your image under 3MB?' });
    return {};
  }
};

export const removeUserHeader = async (userId) => {
  try {
    const res = await authDelete({
      url: `/v2/users/${userId}/profile/header`,
    });

    return res.data;
  } catch (error) {
    pushSmartNotification({ error: 'Could not remove profile header.' });
    return {};
  }
};

export const getUserPosts = async (userId, page = 1, hideNsfw = false) => {
  const res = await axios.get(
    `${config.apiHost}/user/${userId}/posts/${page}${hideNsfw ? '?hideNsfw=1' : ''}`
  );
  const { data } = res;
  return data;
};

export const getUserThreads = async (userId, page = 1, hideNsfw = false) => {
  const res = await axios.get(
    `${config.apiHost}/user/${userId}/threads/${page}${hideNsfw ? '?hideNsfw=1' : ''}`
  );
  const { data } = res;
  return data;
};

export function setupUserFlags(user) {
  const newUser = { ...user };
  if (user.id === null) {
    newUser.loggedIn = false;
  } else {
    newUser.loggedIn = true;
  }

  newUser.isModerator = MODERATOR_ROLES.includes(newUser.role?.code);

  return newUser;
}

export const syncData = async () => {
  try {
    const results = await authGet({
      url: '/user/syncData',
      data: {},
      headers: { 'Cache-Control': 'private, no-store, max-age=0' },
    });

    if (!results || (results && results.data.error)) {
      throw new Error('Could not fetch your user data.');
    }

    const userData = results.data;

    // handle bans
    if (userData.isBanned) {
      saveBannedMessageToStorage(userData.banInfo);
    } else {
      clearBannedMessageFromStorage();
    }

    if (!userData) {
      throw new Error('Could not fetch data.');
    }

    // update local user data
    saveUserToStorage({
      user: {
        id: userData.id,
        username: userData.username,
        avatarUrl: userData.avatarUrl,
        backgroundUrl: userData.backgroundUrl,
        createdAt: userData.createdAt,
        role: userData.role,
        title: userData.title,
      },
    });

    return results.data;
  } catch (err) {
    console.error(err);

    return { error: true };
  }
};

export const updateProfileRatingsDisplay = async (hideProfileRatings) => {
  try {
    const res = await authPut({
      url: '/user/updateProfileRatingsDisplay',
      data: {
        hideProfileRatings,
      },
    });

    return pushSmartNotification(res.data);
  } catch (error) {
    pushSmartNotification(error);
    return {};
  }
};

export const getProfileRatingsDisplay = async () => {
  try {
    const res = await authGet({
      url: '/user/profileRatingsDisplay',
      data: {},
    });
    return res.data.ratingsHiddenForUser;
  } catch (error) {
    pushSmartNotification(error);
    return false;
  }
};

export const deleteOwnAccount = async () => {
  try {
    const { id } = loadUserFromStorage();

    const results = await authDelete({ url: `/user/${id}`, data: {} });

    return results.data;
  } catch (err) {
    console.error(err);

    return { error: true };
  }
};
