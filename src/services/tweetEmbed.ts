import axios from 'axios';
import config from '../../config';

export default async (tweetUrl: string) => {
  const res = await axios.get(`${config.apiHost}/tweet-embed`, { params: { url: tweetUrl } });
  const { data } = res;

  return data;
};
