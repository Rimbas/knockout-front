import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {
  ThemeBackgroundDarker,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { minimalDateFormat } from '../../../utils/dateFormat';

dayjs.extend(relativeTime);

export const StyledDeletedPost = styled.div`
  display: grid;
  position: relative;
  margin-bottom: calc(${ThemeVerticalPadding} / 2);
  font-size: ${ThemeFontSizeMedium};

  .post-toolbar {
    background: ${ThemeBackgroundDarker};
    opacity: 0.5;
    line-height: 30px;
    padding: 0 ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .post-details-date {
    display: inline-flex;
  }

  .thread-post-number {
    margin-left: ${ThemeHorizontalPadding};
    opacity: 0.5;
  }
`;

const DeletedPost = ({ postThreadPostNumber, postDate, postId }) => {
  const postDateHuman = dayjs().isBefore(dayjs(postDate)) ? 'Now' : dayjs().to(dayjs(postDate));

  const postDateHumanMinimal =
    postDateHuman !== 'Now' ? minimalDateFormat(dayjs(postDate).fromNow(true), false) : 'Now';

  return (
    <StyledDeletedPost id={`post-${postId}`}>
      <div className="post-toolbar">
        <span className="post-details-date">
          <span className="post-date-human">{`${postDateHumanMinimal} (deleted)`}</span>
          <span className="thread-post-number">{`#${postThreadPostNumber}`}</span>
        </span>
      </div>
    </StyledDeletedPost>
  );
};

DeletedPost.propTypes = {
  postThreadPostNumber: PropTypes.number.isRequired,
  postDate: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
};

export default DeletedPost;
