import React from 'react';
import styled from 'styled-components';
import { pushSmartNotification } from '../../../utils/notification';
import { FieldLabel, FieldLabelSmall } from '../../../components/FormControls';
import SettingsToggle from '../../../components/SettingsToggle';
import { Settings, updateSettings } from '../../../state/settings';
import { useAppDispatch, useAppSelector } from '../../../state/hooks';

interface OptionsStorageInputProps {
  storageKey: keyof Omit<Settings, 'motdDismissed'>;
  desc: string;
  label: string;
  onChange?: (value: boolean) => void;
  icon: string;
}

const OptionsStorageInput = ({
  storageKey,
  desc,
  label,
  onChange = () => {},
  icon,
}: OptionsStorageInputProps) => {
  const value = useAppSelector((state) => state.settings[storageKey]);
  const dispatch = useAppDispatch();

  const updateValue = (newValue) => {
    dispatch(updateSettings({ [storageKey]: newValue }));
    pushSmartNotification({ message: 'Profile preference saved successfully.' });
  };

  return (
    <SettingsToggle
      label={label}
      desc={desc}
      value={value}
      icon={icon}
      setValue={() => {
        updateValue(!value);
        if (onChange) {
          onChange(!value);
        }
      }}
    />
  );
};

export const StyledOptionsStorageInput = styled.div`
  display: flex;
  justify-content: space-between;
  line-height: normal;
  align-items: center;
  margin-bottom: 20px;
  padding: 0 20px;

  ${FieldLabel} {
    margin-bottom: 5px;
    font-weight: 600;
  }

  ${FieldLabelSmall} {
    margin-bottom: 0px;
  }
`;

export default OptionsStorageInput;
