import React from 'react';
import styled from 'styled-components';
import OptionsStorageInput, { StyledOptionsStorageInput } from './OptionsStorageInput';
import ServerSideInput from './ServerSideInput';

import { getProfileRatingsDisplay, updateProfileRatingsDisplay } from '../../../services/user';
import { updateWidth } from '../../../state/style';
import { Panel, PanelTitle } from '../../../components/Panel';
import { setWidthToStorage } from '../../../services/theme';
import { ThemeTextColor } from '../../../utils/ThemeNew';
import { FieldLabelWithIcon, FieldLabelSmall } from '../../../components/FormControls';
import { useAppDispatch, useAppSelector } from '../../../state/hooks';

interface ThemeInputContainerProps {
  update: (value: string) => void;
  value: string;
  label: string;
  options: { key: string; label: string }[];
  desc: string;
}

const ThemeInputContainer = ({ label, value, options, desc, update }: ThemeInputContainerProps) => {
  return (
    <StyledOptionsStorageInput>
      <div className="options-info">
        <FieldLabelWithIcon label={label} icon="fa-arrows-alt-h" />
        <FieldLabelSmall>{desc}</FieldLabelSmall>
      </div>
      <div className="dropdown">
        <select onChange={(e) => update(e.target.value)} defaultValue={value}>
          {options.map((option) => {
            return (
              <option key={option.key} value={option.key}>
                {option.label}
              </option>
            );
          })}
        </select>
      </div>
    </StyledOptionsStorageInput>
  );
};

const StyledUserExperienceContainer = styled.div`
  .divider {
    margin: 20px 20px;
    color: ${ThemeTextColor};
    opacity: 0.3;
  }
`;

const UserExperienceContainer = () => {
  const width = useAppSelector((state) => state.style.width);
  const dispatch = useAppDispatch();

  const setWidth = (widthValue) => {
    dispatch(updateWidth(widthValue));
    setWidthToStorage(widthValue);
  };

  return (
    <StyledUserExperienceContainer>
      <Panel>
        <PanelTitle title="Because you're important to us <3">User Experience</PanelTitle>
        <div className="options-wrapper">
          <OptionsStorageInput
            desc="Subscribe to threads on reply / on creation"
            label="AutoSub™"
            icon="fa-bell"
            storageKey="autoSubscribe"
          />

          <OptionsStorageInput
            desc="Hide media from new or inactive users"
            label="EyeSafe™"
            icon="fa-eye-slash"
            storageKey="hideRiskyMedia"
          />

          <OptionsStorageInput
            desc="Show which country you're posting from"
            label="FlagPunchy™"
            icon="fa-flag"
            storageKey="displayCountryInfo"
          />

          <OptionsStorageInput
            desc="Hides NSFW threads"
            label="WorkSafe™"
            icon="fa-briefcase"
            storageKey="nsfwFilter"
          />

          <OptionsStorageInput
            desc="Automatically mark threads as read when you view the last page"
            label="MarkThreadRead™"
            icon="fa-check-double"
            storageKey="markLastPageRead"
          />

          <hr className="divider" />

          <ServerSideInput
            desc="Hide ratings displayed on your profile"
            label="BoxHide™"
            icon="fa-box"
            setValue={updateProfileRatingsDisplay}
            getValue={getProfileRatingsDisplay}
          />

          <OptionsStorageInput
            desc="Hide all ratings on the site"
            label="BoxHide Deluxe™"
            icon="fa-boxes-stacked"
            storageKey="hideRatings"
          />

          <OptionsStorageInput
            desc="See who left that *dumb* rating"
            label="Ratings X-ray™"
            icon="fa-x-ray"
            storageKey="ratingsXray"
          />

          <hr className="divider" />

          <OptionsStorageInput
            desc="Enable holiday themes"
            label="HolidayTheme™"
            storageKey="holidayTheme"
            icon="fa-snowflake"
            onChange={() => window.location.reload()}
          />

          <OptionsStorageInput
            desc="Experimental and buggy settings"
            label="PunchyLabs™"
            icon="fa-flask"
            storageKey="punchyLabs"
            onChange={() => window.location.reload()}
          />

          <OptionsStorageInput
            desc="Sticky header that's always with you as you scroll"
            label="StickyHeader™"
            icon="fa-sticky-note"
            storageKey="stickyHeader"
          />

          <OptionsStorageInput
            desc="Show short GIF 'thread advertisements' on the front page"
            label="ThreadAds™"
            icon="fa-ad"
            storageKey="threadAds"
          />

          <ThemeInputContainer
            label="Width"
            desc="Width of the middle section"
            update={setWidth}
            options={[
              { key: 'full', label: 'Full' },
              { key: 'verywide', label: 'Very Wide' },
              { key: 'wide', label: 'Wide' },
              { key: 'medium', label: 'Medium' },
              { key: 'narrow', label: 'Narrow' },
            ]}
            value={width}
          />
        </div>
      </Panel>
    </StyledUserExperienceContainer>
  );
};
export default UserExperienceContainer;
