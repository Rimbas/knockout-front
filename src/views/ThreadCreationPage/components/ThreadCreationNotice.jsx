import React from 'react';
import PropTypes from 'prop-types';

const ThreadCreationNotice = ({ subforumRulesLink, subforumName }) => (
  <div className="thread-creation-notice">
    <h2>Don&apos;t get knocked out!</h2>
    <p>
      Before submitting your thread, please make sure you&apos;ve read and understood the&nbsp;
      <a href="/rules" title="Knockout Rules" target="_blank">
        Knockout Rules
      </a>
      ,&nbsp;
      <a href={subforumRulesLink} title="Subforum Rules" target="_blank">
        {subforumName} Rules
      </a>
      , and&nbsp;
      <a href="/rules/privacy-policy" title="Privacy Policy" target="_blank">
        Privacy Policy.
      </a>
    </p>
  </div>
);

ThreadCreationNotice.propTypes = {
  subforumRulesLink: PropTypes.string.isRequired,
  subforumName: PropTypes.string.isRequired,
};

export default ThreadCreationNotice;
