import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { transparentize } from 'polished';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { Conversation, User } from 'knockout-schema';
import UserAvatar from '../../../components/Avatar';
import { Button, TextButton } from '../../../components/Buttons';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import {
  ThemeFontSizeMedium,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeKnockoutRed,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import getConversationUser from '../getConversationUser';
import getLighterColor from '../getLighterColor';
import { getUser } from '../../../services/user';
import { minimalDateFormat } from '../../../utils/dateFormat';
import { useAppSelector } from '../../../state/hooks';
import { archiveConversation } from '../../../services/messages';
import Modal from '../../../components/Modals/Modal';
import { pushSmartNotification } from '../../../utils/notification';

const StyledMessageMenu = styled.div`
  .messages-header {
    justify-content: space-between;
  }
`;

interface ConversationItemProps {
  selected?: boolean;
  unread?: boolean;
}

const ConversationItem = styled.div<ConversationItemProps>`
  display: flex;
  align-items: center;
  padding: calc(${ThemeVerticalPadding} * 2.5) calc(${ThemeHorizontalPadding} * 4);
  cursor: pointer;
  user-select: none;
  background: ${(props) => {
    if (!props.selected) return 'inherit';
    return getLighterColor(props);
  }};

  &:hover,
  &:focus {
    ${(props) => !props.selected && `background:${transparentize(0.8, getLighterColor(props))};`}

    .archive {
      display: flex;
    }
  }

  .unread-icon {
    display: inline-block;
    width: 9px;
    height: 9px;
    background: ${ThemeHighlightWeaker};
    border-radius: 50%;
    margin-right: calc(${ThemeVerticalPadding} * 1.25);
    margin-left: calc(${ThemeVerticalPadding} * -2.5);
  }

  .details {
    flex-grow: 1;
    overflow: hidden;
  }

  .avatar {
    margin-right: 15px;
    width: 45px;
    max-height: unset;
    background: rgba(0, 0, 0, 0.1);
  }

  .info {
    margin-bottom: 7px;
    display: flex;
    justify-content: space-between;
  }

  .user {
    font-weight: bold;
  }

  .body {
    display: flex;
  }

  .content {
    --max-lines: 3;
    --lh: 1.2rem;
    ${(props) => !props.unread && 'opacity: 0.66;'}
    ${(props) => props.unread && 'font-weight: bold;'}
    font-size: ${ThemeFontSizeMedium};
    line-height: var(--lh);
    position: relative;
    max-height: calc(var(--lh) * var(--max-lines));
    overflow: hidden;
    overflow-wrap: break-word;
    padding-right: 1rem;
  }

  .time {
    opacity: 0.66;
    font-size: ${ThemeFontSizeMedium};
  }

  .archive {
    font-size: ${ThemeFontSizeMedium};
    display: none;
    align-items: flex-end;
    margin-left: auto;
    margin-right: 0;

    button {
      padding: 0;
      background: none;
      border: none;
      cursor: pointer;
      color: ${ThemeTextColor};

      &:hover,
      &:focus {
        color: ${ThemeKnockoutRed};
      }
    }
  }
`;

dayjs.extend(relativeTime);

interface MessageMenuProps {
  getData: () => void;
  setModalOpen: (modalOpen: boolean) => void;
  conversations: Conversation[];
  currentConversation?: {
    id: number;
  };
  loaded: boolean;
  startConversation: (user: User) => void;
}

const MessageMenu: React.FC<MessageMenuProps> = ({
  getData,
  setModalOpen,
  conversations,
  currentConversation = undefined,
  loaded,
  startConversation,
}) => {
  const currentUser = useAppSelector((state) => state.user);
  const history = useHistory();
  const match = useRouteMatch<{ user: string }>();
  const [archivingConversationId, setArchivingConversationId] = useState<number | null>(null);

  const fetchAndCreateConversation = async (userId) => {
    const user = await getUser(userId);
    startConversation(user);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (match.params.user && loaded) {
      fetchAndCreateConversation(match.params.user);
    }
  }, [loaded]);

  const handleArchiveSubmitClick = async () => {
    await archiveConversation(archivingConversationId!);
    getData();
    setArchivingConversationId(null);
    pushSmartNotification({ message: 'Conversation deleted.' });
  };

  return (
    <StyledMessageMenu className="column">
      <div className="messages-header">
        <span className="messages-title">Messages</span>
        <TextButton className="header-button" onClick={() => setModalOpen(true)}>
          <i className="header-icon fa-solid fa-plus" />
        </TextButton>
      </div>
      <div className="messages-body">
        {conversations.length === 0 && (
          <div className="empty-message">
            <div className="empty-title">No messages</div>
            <div className="empty-desc">
              Conversations between you and other users will appear here.
            </div>
            <Button onClick={() => setModalOpen(true)}>Start a conversation</Button>
          </div>
        )}
        {conversations.reduce((conversationItems, conversation) => {
          const user = getConversationUser(conversation.users, currentUser);
          if (!user) {
            return conversationItems;
          }

          const unread =
            conversation.messages?.[0]?.user.id !== currentUser.id &&
            conversation.messages?.[0]?.readAt === null;
          let message = conversation.messages?.[0]?.content;
          if (message && conversation.messages[0].user.id === currentUser.id) {
            message = `You: ${message}`;
          }

          const selectConversation = () =>
            history.push(
              `/${match.url.split('/')[1]}/${conversation.id < 0 ? 'new' : ''}${conversation.id}`
            );
          conversationItems.push(
            <ConversationItem
              key={conversation.id}
              selected={currentConversation?.id === conversation.id}
              onClick={selectConversation}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  selectConversation();
                }
              }}
              unread={unread}
              tabIndex={0}
            >
              {unread && <div className="unread-icon" title="Unread" />}
              <UserAvatar className="avatar" src={user.avatarUrl} />
              <div className="details">
                <div className="info">
                  <UserRoleWrapper user={user} className="user">
                    {user.username}
                  </UserRoleWrapper>
                  <div className="time">
                    {minimalDateFormat(dayjs(conversation.updatedAt).fromNow(true), false)}
                  </div>
                </div>
                <div className="body">
                  <div className="content">
                    {message}
                    {message === undefined && <i className="new-message">New message</i>}
                  </div>
                  <div className="archive">
                    <button
                      type="button"
                      title="Delete Conversation"
                      tabIndex={0}
                      onClick={(e) => {
                        e.stopPropagation();
                        setArchivingConversationId(conversation.id);
                      }}
                    >
                      <i className="fa-solid fa-trash" />
                    </button>
                  </div>
                </div>
              </div>
            </ConversationItem>
          );

          return conversationItems;
        }, [] as JSX.Element[])}
      </div>
      <Modal
        title="Delete conversation"
        isOpen={archivingConversationId !== null}
        iconUrl="/static/icons/siren.png"
        onClose={() => setArchivingConversationId(null)}
        cancelText="Cancel"
        cancelFn={() => setArchivingConversationId(null)}
        submitText="Delete"
        submitFn={handleArchiveSubmitClick}
      >
        <p>Are you sure you want to delete this conversation? This action cannot be undone.</p>
      </Modal>
    </StyledMessageMenu>
  );
};

export default MessageMenu;
