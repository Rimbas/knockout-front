import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { roleCheck } from '../../components/UserRoleRestricted';
import ModerationDashboard from './components/ModerationDashboard';
import ModerationUsers from './components/ModerationUsers';
import ModerationIpLookup from './components/ModerationIpLookup';
import ModerationTags from './components/ModerationTags';
import ModerationMotd from './components/ModerationMotd';
import LinkedTabs from '../../components/Tabs/LinkedTabs';
import ModerationReports from './components/ModerationReports';
import ModerationAdmin from './components/ModerationAdmin';
import { ThemeHorizontalPadding } from '../../utils/ThemeNew';
import { MODERATOR_ROLES } from '../../utils/roleCodes';

const ModeratePage = () => {
  const { path } = useRouteMatch();

  if (!roleCheck(MODERATOR_ROLES)) {
    return <Redirect to="/" />;
  }

  const tabs = [
    { name: 'Dashboard', path, exact: true },
    { name: 'Reports', path: `${path}/reports` },
    { name: 'Users', path: `${path}/users` },
    { name: 'IP Lookup', path: `${path}/iplookup` },
    { name: 'Tags', path: `${path}/tags` },
    { name: 'MOTD', path: `${path}/motd` },
    { name: 'Admin', path: `${path}/admin` },
  ];
  return (
    <>
      <Helmet>
        <title>Moderation - Knockout!</title>
      </Helmet>
      <ModerationContainer>
        <LinkedTabs tabs={tabs} centered />
        <Switch>
          <Route exact path={path}>
            <ModerationDashboard />
          </Route>
          <Route path={`${path}/users`}>
            <ModerationUsers />
          </Route>
          <Route path={`${path}/iplookup`}>
            <ModerationIpLookup />
          </Route>
          <Route path={`${path}/tags`}>
            <ModerationTags />
          </Route>
          <Route path={`${path}/reports`}>
            <ModerationReports />
          </Route>
          <Route path={`${path}/motd`}>
            <ModerationMotd />
          </Route>
          <Route path={`${path}/admin`}>
            <ModerationAdmin />
          </Route>
        </Switch>
      </ModerationContainer>
    </>
  );
};

const ModerationContainer = styled.div`
  padding: 0 ${ThemeHorizontalPadding};
`;
export default ModeratePage;
