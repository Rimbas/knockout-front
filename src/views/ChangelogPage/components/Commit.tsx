/* eslint-disable react/forbid-prop-types */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import styled from 'styled-components';
import { KnockoutCommit } from 'knockout-schema';
import { transparentize } from 'polished';
import { Link } from 'react-router-dom';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeHighlightStronger,
} from '../../../utils/ThemeNew';
import { minimalDateFormat } from '../../../utils/dateFormat';

dayjs.extend(relativeTime);

const DEPLOY_COMMIT_TITLE = "Merge branch 'qa' into 'master'";

interface StyledCommitProps {
  isDeployment: boolean;
}

const StyledCommit = styled.div<StyledCommitProps>`
  display: flex;
  flex-direction: row;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  background: ${(props) =>
    props.isDeployment
      ? transparentize(0.8, ThemeHighlightStronger(props))
      : ThemeBackgroundLighter(props)};

  .commit-info {
    display: flex;
    flex-direction: column;
  }

  .date-and-project-name,
  .title-and-diff-link {
    display: flex;
  }

  .date-and-project-name {
    font-size: ${ThemeFontSizeSmall};
    padding-top: ${ThemeVerticalPadding};
    opacity: 0.75;
  }

  .title-and-diff-link {
    padding-top: 2px;
  }

  .spacer-bead {
    vertical-align: baseline;
    margin: 0 calc(${ThemeHorizontalPadding} / 2) 0 calc(${ThemeHorizontalPadding} / 2);
  }

  .diff-link {
    padding-left: calc(${ThemeHorizontalPadding} / 2);
    font-size: calc(${ThemeFontSizeSmall} * 0.8);
    vertical-align: middle;
  }

  .icon {
    display: flex;
    align-items: center;
    justify-content: center;
    padding-right: ${ThemeHorizontalPadding};

    img {
      width: 100%;
      width: 40px;
      height: 40px;
    }
  }
`;

const Commit = ({ date, projectName, title, url, userId, authorName }: KnockoutCommit) => {
  const formattedDate = minimalDateFormat(dayjs(date).fromNow(true), false);
  const isDeployment = title === DEPLOY_COMMIT_TITLE;
  const iconSrc = isDeployment
    ? 'https://img.icons8.com/color/96/000000/run-command.png'
    : 'https://img.icons8.com/color/96/000000/merge-git.png';

  const userLink = userId ? `/user/${userId}` : null;

  return (
    <StyledCommit isDeployment={isDeployment} title={isDeployment ? 'Deployment' : ''}>
      <div className="icon">
        <img src={iconSrc} alt="Commit" />
      </div>
      <div className="commit-info">
        <div className="title-and-diff-link">
          <a href={url} className="title">
            {title}
          </a>
        </div>
        <div className="date-and-project-name">
          <div className="date" title={date}>
            {formattedDate}
          </div>
          {authorName && (
            <>
              <span className="spacer-bead">•</span>
              {userLink ? <Link to={userLink}>{authorName}</Link> : authorName}
            </>
          )}
          <span className="spacer-bead">•</span>
          <div className="project-name" title="Project">
            {projectName}
          </div>
          {isDeployment && (
            <>
              <span className="spacer-bead">•</span>
              Deployment
            </>
          )}
        </div>
      </div>
    </StyledCommit>
  );
};

export default Commit;
