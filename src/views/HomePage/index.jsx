import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { getSubforumList } from '../../services/subforums';
import SubforumItem from './components/SubforumItem';
import { HomePageSubforumContainer, HomePageWrapper } from './components/HomePageLayout';
import LatestAndPopular from './components/LatestAndPopular';
import UpcomingEvents from './components/UpcomingEvents';

const HomePage = () => {
  const nsfwFilterEnabled = useSelector((state) => state.settings.nsfwFilter);
  const placeholderSubforum = {
    id: 0,
    name: 'Loading subforums...',
    description: 'Patience is a virtue 🙏',
    placeholder: true,
    icon: 'https://img.icons8.com/color/96/000000/filled-chat.png',
    lastPost: { thread: {} },
  };

  const placeholderSubforumList = [
    { ...placeholderSubforum, index: 0, id: -0 },
    { ...placeholderSubforum, index: 1, id: -1 },
    { ...placeholderSubforum, index: 2, id: -2 },
    { ...placeholderSubforum, index: 3, id: -3 },
    { ...placeholderSubforum, index: 4, id: -4 },
    { ...placeholderSubforum, index: 5, id: -5 },
    { ...placeholderSubforum, index: 6, id: -6 },
    { ...placeholderSubforum, index: 7, id: -7 },
    { ...placeholderSubforum, index: 8, id: -8 },
    { ...placeholderSubforum, index: 9, id: -9 },
    { ...placeholderSubforum, index: 10, id: -10 },
    { ...placeholderSubforum, index: 11, id: -11 },
    { ...placeholderSubforum, index: 12, id: -12 },
    { ...placeholderSubforum, index: 13, id: -13 },
  ];

  const [subforums, setSubforums] = useState(placeholderSubforumList);

  useEffect(() => {
    const getSubforums = async () => {
      const subforumsList = await getSubforumList(nsfwFilterEnabled);
      setSubforums(subforumsList);
    };
    getSubforums();
  }, []);

  return (
    <HomePageWrapper>
      <Helmet>
        <title>Knockout!</title>
      </Helmet>
      <div>
        <UpcomingEvents />
        <LatestAndPopular subforumList={subforums} />
      </div>
      <HomePageSubforumContainer>
        {subforums.map((subforum, i) => (
          <SubforumItem
            key={subforum.id}
            index={i}
            createdAt={subforum.createdAt}
            description={subforum.description}
            icon={subforum.icon}
            iconId={subforum.iconId}
            id={subforum.id}
            lastPostId={subforum.lastPostId}
            lastPost={subforum.lastPost}
            name={subforum.name}
            totalPosts={subforum.totalPosts}
            totalThreads={subforum.totalThreads}
            updatedAt={subforum.updatedAt}
            isPlaceholder={subforum.placeholder}
          />
        ))}
      </HomePageSubforumContainer>
    </HomePageWrapper>
  );
};

export default HomePage;
