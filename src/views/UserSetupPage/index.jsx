import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import StyleWrapper from '../../components/Rules/components/StyleWrapper';
import PrivacyPolicy from '../RulesPage/components/PrivacyPolicy';
import Rules from '../../components/Rules';
import UsernameForm from './components/UsernameForm';
import PageSwitcher from './components/PageSwitcher';

import { updateUser } from '../../services/user';
import { userLogin } from '../../state/user/actions';
import { getSiteRules } from '../../services/rules';

import { ThemeHorizontalPadding } from '../../utils/ThemeNew';

const StyledUserSetup = styled.div`
  padding: calc(${ThemeHorizontalPadding} * 3);
`;

const submitUsername = async ({ username, user, history, dispatch }) => {
  try {
    await updateUser({ username }, user.id);
    const updatedUser = { ...user, username };
    userLogin(updatedUser, history, dispatch);
    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};

const UserSetup = ({ user, history }) => {
  const [page, setPage] = useState(1);

  if (user.username) {
    return <Redirect to="/" />;
  }

  const nextPage = () => {
    setPage(page + 1);
    document.activeElement.blur();
    window.scrollTo({
      top: 0,
      left: 0,
    });
  };

  const prevPage = () => {
    setPage(page - 1);
    document.activeElement.blur();
    window.scrollTo({
      top: 0,
      left: 0,
    });
  };

  const submitUser = async (username, dispatch) => {
    return submitUsername({ username, user, history, dispatch });
  };

  let currentPage;
  switch (page) {
    case 1:
      currentPage = (
        <StyleWrapper>
          <PrivacyPolicy />
        </StyleWrapper>
      );
      break;
    case 2:
      currentPage = (
        <StyleWrapper>
          <Rules getRules={getSiteRules} />
        </StyleWrapper>
      );
      break;
    case 3:
      currentPage = (
        <StyleWrapper>
          <UsernameForm submitUsername={submitUser} />
        </StyleWrapper>
      );
      break;
    default: {
      setPage(1);
      currentPage = (
        <StyleWrapper>
          <PrivacyPolicy />
        </StyleWrapper>
      );
    }
  }

  return (
    <StyledUserSetup>
      <Helmet>
        <title>User Setup - Knockout!</title>
      </Helmet>
      {currentPage}
      <PageSwitcher currentPage={page} pageAmount={3} nextPage={nextPage} prevPage={prevPage} />
    </StyledUserSetup>
  );
};

UserSetup.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

UserSetup.defaultProps = {
  user: {
    username: undefined,
  },
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default withRouter(connect(mapStateToProps)(UserSetup));
