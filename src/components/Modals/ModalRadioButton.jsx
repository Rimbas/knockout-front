import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { ThemeFontSizeMedium, ThemeTextColor, ThemeHighlightWeaker } from '../../utils/ThemeNew';

const ModalRadioButton = ({ name, property, values, onChange }) =>
  values.map((value) => (
    <StyledModalRadioButton key={value}>
      <input
        type="radio"
        id={value.toString()}
        name={name}
        value={value}
        checked={property === value}
        onChange={(e) => onChange(e)}
      />
      <label htmlFor={value.toString()} className="radio-button-label">
        <div className="radio-button">
          <div className="radio-button-outer" />
          <div className="radio-button-inner" />
        </div>
        {value}
      </label>
    </StyledModalRadioButton>
  ));

export default ModalRadioButton;

ModalRadioButton.propTypes = {
  name: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  values: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired,
};

const StyledModalRadioButton = styled.div`
  font-size: ${ThemeFontSizeMedium};
  margin-bottom: 10px;

  .radio-button-label {
    display: block;
    text-transform: capitalize;

    .radio-button {
      position: relative;
      width: 24px;
      height: 24px;
      display: inline-block;
      vertical-align: middle;
      margin-right: 8px;

      .radio-button-outer {
        border-radius: 50%;
        border: 2px solid ${(props) => transparentize(0.4, ThemeTextColor(props))};
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        box-sizing: border-box;
        transition: 0.4s;
      }

      .radio-button-inner {
        position: absolute;
        top: 0;
        left: 0;
        border-radius: 50%;
        background: ${ThemeHighlightWeaker};
        width: 100%;
        height: 100%;
        box-sizing: border-box;
        transform: scale(0.5);
        opacity: 0;
        transition: 0.4s;
      }
    }
  }
  input[type='radio'] {
    display: none;
    font-size: ${ThemeFontSizeMedium};

    &:checked {
      ~ .radio-button-label .radio-button {
        position: relative;
        width: 24px;
        height: 24px;
        display: inline-block;

        .radio-button-outer {
          border-color: ${ThemeHighlightWeaker};
        }

        .radio-button-inner {
          opacity: 1;
        }
      }
    }
  }
`;
