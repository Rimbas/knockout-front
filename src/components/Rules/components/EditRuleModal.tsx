import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Rule } from 'knockout-schema';
import Modal from '../../Modals/Modal';
import { FieldLabel, FieldLabelSmall, TextField } from '../../FormControls';
import {
  ThemeEditorFontFamily,
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeFontFamily,
} from '../../../utils/ThemeNew';

const StyledEditRuleModalContent = styled.div`
  .cardinality {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeMedium};
    font-family: ${ThemeFontFamily};
    line-height: 1.1;
    margin-bottom: 20px;
    border: none;
    width: 100%;
    box-sizing: border-box;
    background: ${ThemeBackgroundLighter};
    resize: none;
  }

  textarea {
    width: 100%;
    height: 100px;
    border: none;
    border-radius: 0;
    box-sizing: border-box;
    font-family: ${ThemeEditorFontFamily};
    background: ${ThemeBackgroundLighter};
    color: ${ThemeTextColor};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    font-size: ${ThemeFontSizeMedium};
    line-height: calc(${ThemeFontSizeMedium} * 1.2);
  }
`;

interface EditRuleModalProps {
  isOpen: boolean;
  closeModal: () => void;
  rule: Rule;
  submitFn: (category: string, title: string, description: string) => Promise<void>;
}

const EditRuleModal = ({ isOpen, closeModal, rule, submitFn }: EditRuleModalProps) => {
  const [category, setCategory] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    setCategory(rule.category);
    setTitle(rule.title);
    setDescription(rule.description);
  }, [rule]);

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Edit Rule"
      cancelFn={closeModal}
      submitFn={() => {
        submitFn(category, title, description);
        closeModal();
      }}
      isOpen={isOpen}
    >
      <StyledEditRuleModalContent>
        <FieldLabel>Title</FieldLabel>
        <FieldLabelSmall>Between 3 and 140 characters</FieldLabelSmall>
        <TextField
          aria-label="edit-rule-title"
          value={title}
          minLength={3}
          maxLength={140}
          onChange={(e) => setTitle(e.target.value)}
        />
        <FieldLabel>Category</FieldLabel>
        <FieldLabelSmall>Between 3 and 140 characters</FieldLabelSmall>
        <TextField
          aria-label="edit-rule-category"
          value={category}
          minLength={3}
          maxLength={140}
          onChange={(e) => setCategory(e.target.value)}
        />
        <FieldLabel>Description</FieldLabel>
        <FieldLabelSmall>Between 10 and 1000 characters</FieldLabelSmall>
        <textarea
          value={description}
          minLength={10}
          maxLength={1000}
          onChange={(e) => setDescription(e.target.value)}
          aria-label="edit-rule-description"
        />
      </StyledEditRuleModalContent>
    </Modal>
  );
};

export default EditRuleModal;
