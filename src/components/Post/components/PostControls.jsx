/* eslint-disable no-alert */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import LoggedInOnly from '../../LoggedInOnly';
import { roleCheck, UserRoleRestricted } from '../../UserRoleRestricted';
import { ThemeTextColor, ThemeHorizontalPadding } from '../../../utils/ThemeNew';
import BanModal from '../../BanModal';
import { MODERATOR_ROLES } from '../../../utils/roleCodes';

const StyledPostControls = styled.div`
  display: flex;

  .btn-control {
    height: 100%;
    background: transparent;
    color: ${ThemeTextColor};
    opacity: 0.6;
    border: none;
    cursor: pointer;
    padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
    transition: opacity 100ms ease-in-out;
    &:hover {
      opacity: 1;
    }
  }
`;

const PostControls = ({
  user,
  postId,
  showBBCode,
  toggleEditable,
  replyToPost,
  reportPost,
  byCurrentUser,
  threadLocked,
}) => {
  const [banModalOpen, setBanModalOpen] = useState(false);
  const userIsMod = roleCheck(MODERATOR_ROLES);

  const onReportClick = () => {
    reportPost(postId);
  };

  return (
    <LoggedInOnly>
      <StyledPostControls>
        <UserRoleRestricted roleCodes={MODERATOR_ROLES}>
          <button
            className="btn-control"
            type="button"
            onClick={() => setBanModalOpen(true)}
            title="Ban User"
          >
            <i className="fa-solid fa-gavel" />
          </button>
          <BanModal
            userId={user.id}
            postId={postId}
            submitFn={() => setBanModalOpen(false)}
            cancelFn={() => setBanModalOpen(false)}
            isOpen={banModalOpen}
          />
        </UserRoleRestricted>
        {!userIsMod && (
          <button type="button" className="btn-control" onClick={onReportClick} title="Report post">
            <i className="fa-solid fa-flag" />
          </button>
        )}

        {(!byCurrentUser || threadLocked) && (
          <button type="button" className="btn-control" onClick={showBBCode} title="Show BBCode">
            <i className="fa-solid fa-code" />
          </button>
        )}
        {(userIsMod || (byCurrentUser && !threadLocked)) && (
          <button type="button" className="btn-control" onClick={toggleEditable} title="Edit">
            <i className="fa-solid fa-pencil-alt" />
          </button>
        )}
        {(userIsMod || !threadLocked) && (
          <button type="button" className="btn-control" onClick={() => replyToPost()} title="Reply">
            <i className="fa-solid fa-reply" />
          </button>
        )}
      </StyledPostControls>
    </LoggedInOnly>
  );
};
export default PostControls;
PostControls.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  }).isRequired,
  postId: PropTypes.number.isRequired,
  showBBCode: PropTypes.func,
  toggleEditable: PropTypes.func,
  replyToPost: PropTypes.func,
  reportPost: PropTypes.func,
  byCurrentUser: PropTypes.bool,
  threadLocked: PropTypes.bool,
};
PostControls.defaultProps = {
  showBBCode: undefined,
  toggleEditable: undefined,
  replyToPost: undefined,
  reportPost: undefined,
  byCurrentUser: false,
  threadLocked: false,
};
