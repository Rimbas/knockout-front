import { SetStateAction } from 'react';
import { modifySelectionRange } from './selection';
import { emoteExists } from '../../../utils/emotes';

export const EMOTE_CHAR = ':';

function getEmoteStartIndex(content: string, caretPosition: number) {
  let index = caretPosition - 1;
  while (index >= 0 && content[index] !== EMOTE_CHAR) {
    index -= 1;
  }
  return index >= 0 ? index : -1;
}

export function getEmoteQuery(content: string, caretPosition: number) {
  const startIndex = getEmoteStartIndex(content, caretPosition);
  return startIndex >= 0 ? content.substring(startIndex + 1, caretPosition) : '';
}

export function getNewEmoteQuery(content: string, caretPosition: number) {
  const startIndex = getEmoteStartIndex(content, caretPosition);
  const previousIndex = getEmoteStartIndex(content, startIndex - 1) + 1;
  if (startIndex - previousIndex < 20 && emoteExists(content.slice(previousIndex, startIndex))) {
    return '';
  }
  return startIndex >= 0 ? content.substring(startIndex + 1, caretPosition) : '';
}

function removeEmoteStartFragment(content: string, caretPosition: number) {
  const startIndex = getEmoteStartIndex(content, caretPosition);
  if (startIndex >= 0) {
    const mentionQueryLength = getEmoteQuery(content, caretPosition).length + 1; // +1 for the ":"
    const modifiedContent =
      content.substring(0, startIndex) + content.substring(startIndex + mentionQueryLength);
    const newCaretPosition = startIndex;
    return { updatedContent: modifiedContent, newCaret: newCaretPosition };
  }
  return { updatedContent: content, newCaret: caretPosition };
}

export const insertEmote = (
  emote: string,
  input: { selectionStart: number },
  content: string,
  setContent: (value: string) => void,
  setSelectionRange: (value: SetStateAction<number[]>) => void,
  removePlaceholder = false
) => {
  const updatedContent = `${EMOTE_CHAR}${emote}${EMOTE_CHAR} `;
  const mentionLength = updatedContent.length;
  const caretPosition = input.selectionStart;

  let newContent = content;
  let newCaretPosition = caretPosition;

  if (removePlaceholder) {
    const { updatedContent: modifiedContent, newCaret } = removeEmoteStartFragment(
      content,
      caretPosition
    );
    newContent = modifiedContent;
    newCaretPosition = newCaret;
  }

  newContent =
    newContent.substring(0, newCaretPosition) +
    updatedContent +
    newContent.substring(newCaretPosition);

  setContent(newContent);
  newCaretPosition += mentionLength;
  setSelectionRange([newCaretPosition, newCaretPosition]);
  modifySelectionRange(input, newCaretPosition);
};
