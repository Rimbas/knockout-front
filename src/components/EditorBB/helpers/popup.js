/* eslint-disable import/prefer-default-export */
import { getCaretCoordinates } from './selection';

function getLeft(element, caretPos, popupSize) {
  // position popup so that it's in the middle of the caret
  const offsetLeft = caretPos.left - popupSize.width / 2;
  // ensure the popup can't go off to the left or right side of the text area
  const maxLeft = element.clientWidth - popupSize.width;
  const clampedLeft = Math.min(offsetLeft, maxLeft);
  return Math.max(0, clampedLeft);
}

function getTop(caretPos) {
  const offset = -8;
  return caretPos.top + offset;
}

export const getPopupPosition = (element, caretIndex, popupSize) => {
  const caretPos = getCaretCoordinates(element, caretIndex);
  const left = getLeft(element, caretPos, popupSize);
  const top = getTop(caretPos);
  return { top, left };
};
