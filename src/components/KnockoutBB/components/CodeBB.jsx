/* eslint-disable react/forbid-prop-types */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { PrismAsyncLight as SyntaxHighlighter } from 'react-syntax-highlighter';
import { oneDark, oneLight } from 'react-syntax-highlighter/dist/cjs/styles/prism';
import { useSelector } from 'react-redux';
import { pushSmartNotification } from '../../../utils/notification';
import { ThemeTextColor } from '../../../utils/ThemeNew';

const StyledCodeBB = styled.div`
  position: relative;

  ${(props) => props.inline && 'display: inline;'}

  .copy-btn {
    background: none;
    border: none;
    position: absolute;
    top: 4px;
    right: 4px;
    cursor: pointer;

    i {
      color: ${ThemeTextColor};
      opacity: 0.5;

      &:hover {
        opacity: 1;
      }
    }
  }
`;

const StyledCodeblock = styled(SyntaxHighlighter)`
  display: block;
  max-height: 90vh;
  margin: 15px 0;
  box-sizing: border-box;

  overflow-x: auto;
  overflow-y: auto;
  width: 100%;

  code {
    max-width: 100%;
  }
`;

const handleTextCopy = (text) => {
  navigator.clipboard.writeText(text);
  pushSmartNotification({ message: 'Code copied to clipboard.' });
};

const CodeBB = ({ language, inline, children }) => {
  const theme = useSelector((state) => state.style.theme);
  const colorScheme = theme === 'light' ? oneLight : oneDark;
  // These override the element styling provided by SyntaxHighlighter
  const inlineStyle = { display: 'inline', padding: '2px 4px' };

  return (
    <StyledCodeBB inline={inline}>
      {!inline && (
        <button
          type="button"
          className="copy-btn"
          title="Copy code to clipboard"
          onClick={() => handleTextCopy(children)}
        >
          <i className="fas fa-copy" />
        </button>
      )}
      <StyledCodeblock
        showLineNumbers={!inline}
        inline={inline}
        customStyle={inline ? inlineStyle : undefined}
        style={colorScheme}
        language={language}
      >
        {children}
      </StyledCodeblock>
    </StyledCodeBB>
  );
};

CodeBB.propTypes = {
  language: PropTypes.string,
  inline: PropTypes.bool,
  children: PropTypes.any,
};

CodeBB.defaultProps = {
  language: 'plaintext',
  inline: false,
  children: null,
};

export default CodeBB;
