// Mirrored from https://github.com/FixTweet/FxTwitter/blob/main/src/types/types.d.ts

declare enum DataProvider {
  Twitter = 'twitter',
  Bsky = 'bsky',
}

interface TweetAPIResponse {
  code: number;
  message: string;
  tweet?: APITwitterStatus;
}

interface APITranslate {
  text: string;
  source_lang: string;
  source_lang_en: string;
  target_lang: string;
}

interface APIExternalMedia {
  type: 'video';
  url: string;
  thumbnail_url?: string;
  height?: number;
  width?: number;
}

interface APIPollChoice {
  label: string;
  count: number;
  percentage: number;
}

interface APIPoll {
  choices: APIPollChoice[];
  total_votes: number;
  ends_at: string;
  time_left_en: string;
}

interface APIMedia {
  type: string;
  url: string;
  width: number;
  height: number;
}

interface APIPhoto extends APIMedia {
  type: 'photo';
  altText: string;
}

interface APIVideo extends APIMedia {
  type: 'video' | 'gif';
  thumbnail_url: string;
  format: string;
  duration: number;
  variants: TweetMediaFormat[];
}

type TweetMediaFormat = {
  bitrate: number;
  content_type: string;
  url: string;
};

interface APIMosaicPhoto extends APIMedia {
  type: 'mosaic_photo';
  formats: {
    webp: string;
    jpeg: string;
  };
}

interface APIStatus {
  id: string;
  url: string;
  text: string;
  created_at: string;
  created_timestamp: number;

  likes: number;
  reposts: number;
  replies: number;

  quote?: APIStatus;
  poll?: APIPoll;
  author: APIUser;

  media: {
    external?: APIExternalMedia;
    photos?: APIPhoto[];
    videos?: APIVideo[];
    all?: APIMedia[];
    mosaic?: APIMosaicPhoto;
  };

  lang: string | null;
  possibly_sensitive: boolean;

  replying_to?: APITwitterStatus;

  source: string | null;

  embed_card: 'tweet' | 'summary' | 'summary_large_image' | 'player';
  provider: DataProvider;
}

interface APITwitterCommunityNote {
  text: string;
  entities: BirdwatchEntity[];
}

type BirdwatchEntity = {
  fromIndex: number; // 119
  toIndex: number; // 154
  ref: {
    type: 'TimelineUrl';
    url: string; // https://t.co/jxvVatCVCz
    urlType: 'ExternalUrl';
  };
};

interface APITwitterStatus extends APIStatus {
  views?: number | null;
  translation?: APITranslate;

  is_note_tweet: boolean;
  community_note: APITwitterCommunityNote | null;
  provider: DataProvider.Twitter;
}

interface APIUser {
  id: string;
  name: string;
  screen_name: string;
  global_screen_name?: string;
  avatar_url: string;
  banner_url: string;
  // verified: 'legacy' | 'blue'| 'business' | 'government';
  // verified_label: string;
  description: string;
  location: string;
  url: string;
  protected: boolean;
  followers: number;
  following: number;
  statuses: number;
  likes: number;
  joined: string;
  website: {
    url: string;
    display_url: string;
  } | null;
  birthday: {
    day?: number;
    month?: number;
    year?: number;
  };
}
