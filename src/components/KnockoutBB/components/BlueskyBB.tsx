import React, { useEffect, useState } from 'react';
import useAsyncScript from '../../../utils/useAsyncScript';

export const isBluesky = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'bsky.app') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

interface BlueskyBBProps {
  href: string;
}

const BlueskyBB = ({ href }: BlueskyBBProps) => {
  const url = new URL(href);
  const profileName = url.pathname.split('/')[2];
  const postId = url.pathname.split('/')[4];
  const [profileId, setProfileId] = useState('');

  useAsyncScript('https://embed.bsky.app/static/embed.js', '', true, profileId);

  const getProfileId = async () => {
    const result = await fetch(
      `https://public.api.bsky.app/xrpc/com.atproto.identity.resolveHandle?handle=${profileName}`
    );
    setProfileId((await result.json()).did);
  };

  useEffect(() => {
    getProfileId();
  }, []);

  if (!profileId) {
    return null;
  }

  return (
    <blockquote
      className="bluesky-embed"
      data-bluesky-uri={`at://${profileId}/app.bsky.feed.post/${postId}`}
    />
  );
};

export default BlueskyBB;
