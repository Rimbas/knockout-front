import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { rgba } from 'polished';
import { ThemeBodyBackgroundColor, ThemeFontSizeSmall, ThemeTextColor } from '../../utils/ThemeNew';

const Tooltip = ({
  text,
  children,
  className,
  top,
  right,
  left,
  widthLimited = false,
  fullWidth = false,
}) => {
  const [visible, setVisible] = useState(false);
  const [rendered, setRendered] = useState(false);
  const TRANSITION = 100;

  const showTooltip = () => {
    setRendered(true);
    setTimeout(() => {
      setVisible(true);
    }, 10);
  };

  const hideTooltip = () => {
    setVisible(false);
    setTimeout(() => {
      setRendered(false);
    }, TRANSITION);
  };

  return (
    <StyledTooltip
      className={className}
      top={top}
      right={right}
      left={left}
      widthLimited={widthLimited}
      fullWidth={fullWidth}
      visible={visible}
      transition={TRANSITION}
      onMouseEnter={showTooltip}
      onMouseLeave={hideTooltip}
      onFocus={showTooltip}
      onBlur={hideTooltip}
    >
      {rendered && <div className="tooltip-text">{text}</div>}
      {children}
    </StyledTooltip>
  );
};

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  text: PropTypes.string.isRequired,
  top: PropTypes.bool,
  right: PropTypes.bool,
  left: PropTypes.bool,
  widthLimited: PropTypes.bool,
  fullWidth: PropTypes.bool,
  className: PropTypes.string,
};
Tooltip.defaultProps = {
  top: true,
  right: false,
  left: false,
  widthLimited: false,
  fullWidth: false,
  className: '',
};

const StyledTooltip = styled.div`
  position: relative;

  .tooltip-text {
    opacity: ${(props) => (props.visible ? 1 : 0)};
    z-index: 99;
    transition: opacity ${(props) => props.transition}ms ease-in-out;
    pointer-events: none;
    font-size: ${ThemeFontSizeSmall};
    position: absolute;

    ${(props) => (props.top ? `top: -10px` : `bottom: -10px`)};
    left: 50%;

    transform: translate(
      ${(props) => {
        if (props.right) return '-20%';
        if (props.left) return '-80%';
        return '-50%';
      }},
      ${(props) => (props.top ? '-100%' : '100%')}
    );

    padding: 7px;
    background: ${ThemeTextColor};
    color: ${(props) =>
      props.theme.mode === 'light' ? 'white' : rgba(ThemeBodyBackgroundColor(props), 1)};
    font-weight: 600;
    border-radius: 3px;
    width: max-content;

    ${(props) =>
      props.widthLimited &&
      `max-width: 100%;
    box-sizing: border-box;
    word-break: break-all;`}
  }

  ${(props) => props.fullWidth && `width: 100%;`}
`;

export default Tooltip;
