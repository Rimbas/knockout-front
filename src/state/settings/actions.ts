export enum SettingsAction {
  SETTINGS_UPDATE,
}

export interface Settings {
  punchyLabs: boolean;
  latestThreadMode: boolean;
  autoSubscribe: boolean;
  markLastPageRead: boolean;
  ratingsXray: boolean;
  stickyHeader: boolean;
  threadAds: boolean;
  hideRatings: boolean;
  nsfwFilter: boolean;
  holidayTheme: boolean;
  displayCountryInfo: boolean;
  hideRiskyMedia: boolean;
  motdDismissed: number;
}

export function updateSettings(value: Partial<Settings>) {
  return {
    type: SettingsAction.SETTINGS_UPDATE,
    value,
  };
}
